# golog

Idea from [justforfunc #9](https://www.youtube.com/watch?v=LSzR0VEraWw).

```sh
go get gitlab.com/bulldogcreative/golog
```

## Usage

```go
package main

import (
    "log"
    "context"
    "net/http"
    "gitlab.com/bulldogcreative/golog"
)

func handleRequest(w http.ResponseWriter, r *http.Request) {
    ctx := r.Context()
    golog.Print(ctx, "Request", "new request")
    w.Write([]byte("Hi"))
}

func main() {
	log.Println("Starting...")
	http.HandleFunc("/", golog.DecorateHandler(handleRequest))
	http.ListenAndServe(":8181", nil)
}

```
