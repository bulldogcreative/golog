package golog

import (
	"context"
	"log"
	"math/rand"
	"net/http"
)

type key int

const contextKey = key(99)

// Print request with ID
func Print(ctx context.Context, title string, msg string) {
	id := ctx.Value(contextKey)
	log.Printf("[%v] %s: %s\n", id, title, msg)
}

// DecorateHandler with a logger
func DecorateHandler(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		id := rand.Int63()
		ctx = context.WithValue(ctx, contextKey, id)
		f(w, r.WithContext(ctx))
	}
}
